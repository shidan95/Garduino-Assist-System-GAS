# Garduino-Assist-System-GAS

Mobile Application developed using Webview (HTML, Javascrript)

This project is using Android Studio IDE...

System named as Garduino Assist System GAS, but this only part of the system.

All data and transaction of the system that involving worker and garden are implement in this part of the system.

This is example User Interface:

<p>Login GUI</p>
<kbd><img src="https://gitlab.com/shidan95/Garduino-Assist-System-GAS/-/raw/master/GUI/login.png" width="250"></kbd>

<p>Dashboard GUI</p>
<kbd><img src="https://gitlab.com/shidan95/Garduino-Assist-System-GAS/-/raw/master/GUI/dashboard.png" width="250"></kbd>

<h3>Place GUI</h3>
<kbd><img src="https://gitlab.com/shidan95/Garduino-Assist-System-GAS/-/raw/master/GUI/PlaceActivity.png" width="250"></kbd>

<h3>Plant GUI</h3>
<kbd><img src="https://gitlab.com/shidan95/Garduino-Assist-System-GAS/-/raw/master/GUI/PlantActivity.png" width="250"></kbd>

<h3>Worker Activity GUI</h3>
<kbd><img src="https://gitlab.com/shidan95/Garduino-Assist-System-GAS/-/raw/master/GUI/WorkerActivity.png" width="250"></kbd>

<h3>Worker Item GUI</h3>
<kbd><img src="https://gitlab.com/shidan95/Garduino-Assist-System-GAS/-/raw/master/GUI/WorkerItem.png" width="250"></kbd>

<h3>Worker Schedule GUI</h3>
<kbd><img src="https://gitlab.com/shidan95/Garduino-Assist-System-GAS/-/raw/master/GUI/WorkerSchedule.png" width="250"></kbd>

<h3>Feedback GUI</h3>
<kbd><img src="https://gitlab.com/shidan95/Garduino-Assist-System-GAS/-/raw/master/GUI/feedbackActivity.png" width="250"></kbd>

<h3>Update Profile GUI</h3>
<kbd><img src="https://gitlab.com/shidan95/Garduino-Assist-System-GAS/-/raw/master/GUI/updateProfileActivity.png" width="250"></kbd>
